﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Test;

namespace zz.batterymon
{
    public class BatteryInfoProvider
    {
        const int ProcessorInformation = 11;
        const uint STATUS_SUCCESS = 0;

        [StructLayout(LayoutKind.Sequential)]
        struct SYSTEM_BATTERY_STATE
        {
            public byte AcOnLine;
            public byte BatteryPresent;
            public byte Charging;
            public byte Discharging;
            public byte spare1;
            public byte spare2;
            public byte spare3;
            public byte spare4;
            public UInt32 MaxCapacity;
            public UInt32 RemainingCapacity;
            public Int32 Rate;
            public UInt32 EstimatedTime;
            public UInt32 DefaultAlert1;
            public UInt32 DefaultAlert2;
        }

        [DllImport("powrprof.dll", SetLastError = true)]
        private static extern UInt32 CallNtPowerInformation(
            Int32 InformationLevel,
            IntPtr lpInputBuffer,
            UInt32 nInputBufferSize,
            IntPtr lpOutputBuffer,
            UInt32 nOutputBufferSize
        );

        const Int32 SystemBatteryState = 5;

        public int GetBatteryRate0()
        {
            IntPtr status = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(SYSTEM_BATTERY_STATE)));

            try
            {
                uint retval = CallNtPowerInformation(
                    SystemBatteryState,
                    IntPtr.Zero, 
                    0,
                    status,
                    (uint)Marshal.SizeOf<SYSTEM_BATTERY_STATE>());

                if (retval != 0)
                    throw new Win32Exception((int) retval);

                SYSTEM_BATTERY_STATE batt_status = (SYSTEM_BATTERY_STATE)Marshal.PtrToStructure(status, typeof(SYSTEM_BATTERY_STATE));
                return batt_status.Rate;

            }
            finally
            {
                Marshal.FreeHGlobal(status);
            }



        }

        public int GetBatteryRate()
        {
            return  BatteryInfo.GetBatteryInformation().DischargeRate;
        }
    }

}
