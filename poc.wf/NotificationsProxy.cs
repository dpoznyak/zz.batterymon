﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace zz.batterymon
{
    public class NotificationsProxy : IDisposable
    {
        private readonly Settings.PowerPlan[] _plans;
        private NotifyIcon _notifyIcon;
        private string _tooltip;

        public NotificationsProxy(Settings.PowerPlan[] plans)
        {
            _plans = plans;
            _notifyIcon = new NotifyIcon()
            {
                //ContextMenuStrip = new ContextMenuStrip(),
                
//                Text = DefaultTooltip,
                Visible = true
            };

            Status = RagStatus.Green;
            
            //_notifyIcon.ContextMenuStrip.Opening += ContextMenuStrip_Opening;
            _notifyIcon.MouseClick += (sender, args) =>
            {
                if (args.Button == MouseButtons.Right)
                {
                    if (MessageBox.Show("Are you sure you want to exit?", "Exit?", MessageBoxButtons.YesNo) ==
                        DialogResult.Yes)
                        Exit?.Invoke(this, EventArgs.Empty);
                }
            };

            _notifyIcon.DoubleClick += (sender, args) => { _planIndexX.OnNext(_planIndexX.Value + 1); };

            _icons = new ConcurrentDictionary<IconKey, Icon>();

            PlanX = _planIndexX.Select(x => _plans[x % _plans.Length])
                    .DistinctUntilChanged()
                    .Throttle(TimeSpan.FromMilliseconds(50))
                ;

            _statusX
                .DistinctUntilChanged()
                .Throttle(TimeSpan.FromMilliseconds(50))
                .CombineLatest(PlanX, (rag, plan) => new { rag, plan })
                .Subscribe(value =>
                    _notifyIcon.Icon = 
                        
                        _icons.GetOrAdd(new IconKey() { RagStatus = value.rag, PlanName = value.plan.Label}, 

                            k =>
                            {
                                var assembly = typeof(NotificationsProxy).Assembly;
                                var ragIcon = new Icon(assembly
                                    .GetManifestResourceStream(
                                        $"zz.batterymon.icons.battery-{Enum.GetName(typeof(RagStatus), value.rag).ToLower()}.ico"));

                                var planIcon =
                                    assembly.GetManifestResourceStream($"zz.batterymon.icons.plan-{value.plan.Label}.ico");

                                if (planIcon == null) return ragIcon;

                                var bitmap = ragIcon.ToBitmap();
                                var g = Graphics.FromImage(bitmap);
                                g.DrawIcon(new Icon(planIcon), new Rectangle(bitmap.Width / 2, 0, bitmap.Width / 2, bitmap.Height / 2));
                                g.Save();
                                return Icon.FromHandle(bitmap.GetHicon());
                            }));

        }

        public struct IconKey
        {
            public RagStatus RagStatus { get; set; }
            public string PlanName { get; set; }

            public bool Equals(IconKey other)
            {
                return RagStatus == other.RagStatus && string.Equals(PlanName, other.PlanName);
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                return obj is IconKey && Equals((IconKey) obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return ((int) RagStatus * 397) ^ (PlanName != null ? PlanName.GetHashCode() : 0);
                }
            }
        }

        public event EventHandler Exit;

        private BehaviorSubject<RagStatus> _statusX = new BehaviorSubject<RagStatus>(RagStatus.Green);
        private BehaviorSubject<int> _planIndexX = new BehaviorSubject<int>(0);
        private ConcurrentDictionary<IconKey, Icon> _icons;

        public IObservable<Settings.PowerPlan> PlanX { get; }

        public RagStatus Status
        {
            get { return _statusX.Value; }
            set
            {
                _statusX.OnNext( value);
            }
        }

        public string Tooltip
        {
            get { return _tooltip; }
            set
            {
                _tooltip = value;
                _notifyIcon.Text = value;
            }
        }

        public void ReportStatus(RagStatus status, string message)
        {
            Status = status;
            Tooltip = message;
            switch (status)
            {
                case RagStatus.Red:
                    _notifyIcon.ShowBalloonTip(2000, "Battery discharge warning", message, ToolTipIcon.Warning);

                    break;
                case RagStatus.Amber:
                    //_notifyIcon.ShowBalloonTip(1000, "Battery discharge warning", message, ToolTipIcon.Info);

                    break;
             
            }





        }

        public void Dispose()
        {
            _notifyIcon?.Dispose();
        }
    }

    public enum RagStatus
    {
        Green,
        Amber,
        Red
    }
}
