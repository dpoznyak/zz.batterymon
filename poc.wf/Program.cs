﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reactive.Concurrency;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace zz.batterymon
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            var tokenSource = new CancellationTokenSource();
            CancellationToken ct = tokenSource.Token;
            var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText("appsettings.json"));


            using (var prx = new NotificationsProxy(settings.PowerPlans))
            {
                prx.Exit += (sender, args) => tokenSource.Cancel();

                prx.PlanX.ObserveOn(Scheduler.Default)
                    .Subscribe(plan => Process.Start(new ProcessStartInfo{
                        FileName = "powercfg",
                        Arguments = $"/S {plan.Id}",
                        WindowStyle = ProcessWindowStyle.Hidden}).WaitForExit());

                var measures = new Subject<int>();

                MinRunningAverage(measures, settings.A.N).Zip(
                        MinRunningAverage(measures, settings.R.N), (a, r) => new
                        {
                            a,
                            r,
                            da = a - settings.A.DischargeRateThreshold,
                            dr = r - settings.R.DischargeRateThreshold
                        })
                    //.Skip(Math.Min(
                    //    settings.A.N,
                    //    settings.R.N
                    //    ))
                    .Select(x =>
                    {
                        if (x.dr < 0) return new { s = RagStatus.Red, t = $"Red: {x.r}mW" };
                        if (x.da < 0) return new { s = RagStatus.Amber, t = $"Amber: {x.a}mW" };
                        return new { s = RagStatus.Green, t = $"Green: {x.a}mW" };
                    })

                    //.Materialize()
                    //.Do(x => 
                    //    Console.WriteLine(JsonConvert.SerializeObject(x, Formatting.None)))
                    //.Dematerialize()

                    .DistinctUntilChanged(x => x.s)
                    .Subscribe(x =>
                    {
                        prx.ReportStatus(x.s, x.t);
                    });


                Task.Factory.StartNew(() =>
                {
                    while (true)
                    {
                        measures.OnNext(new BatteryInfoProvider().GetBatteryRate());
                        if (ct.IsCancellationRequested)
                            return;
                        Thread.Sleep(settings.PollIntervalMs);
                    }

                }).Wait();
            }


        }

        private static IObservable<int> MinRunningAverage(Subject<int> measures, int[] windowSizes)
        {
            return measures
                //.ObserveOn(Scheduler.Now)
                .Scan(windowSizes.Select(x => new
                {
                    sum = 0,
                    items = new Queue<int>(),
                    windowSize = x,
                }), (aggs, x) => aggs.Select(agg =>
                {
                    var d = x;
                    agg.items.Enqueue(x);
                    if (agg.items.Count > agg.windowSize)
                    {
                        d -= agg.items.Dequeue();
                    }
                    return new {sum = agg.sum + d, agg.items, agg.windowSize};
                }))
                .Select(aggs => aggs.Min(x => x.sum / x.items.Count));
        }


        private static IObservable<int> RunningAverage(Subject<int> measures, int windowSize)
        {
            return measures
                //.ObserveOn(Scheduler.Now)
                .Scan(new
                {
                    sum = 0,
                    items = new Queue<int>(),
                }, (agg, x) =>
                {
                    var d = x;
                    agg.items.Enqueue(x);
                    if (agg.items.Count > windowSize)
                    {
                        d -= agg.items.Dequeue();
                    }
                    return new { sum = agg.sum + d, items = agg.items };
                })
                .Select(x => x.sum / x.items.Count);
        }
    }

    public class Settings
    {
        public class Level
        {
            public int[] N { get; set; }
            public int DischargeRateThreshold { get; set; }
            public bool DoNotAlert { get; set; }
        }

        public Level R { get; set; } = new Level() { DischargeRateThreshold = 20000, N = new[] { 10, 6, 3 } };
        public Level A { get; set; } = new Level() { DischargeRateThreshold = 10000, N = new[] { 10, 6, 3 } };
        public int PollIntervalMs { get; set; } = 1000;

        public class PowerPlan
        {
            public string Label { get; set; }
            public string Id { get; set; }
        }
        public PowerPlan[] PowerPlans { get; set; }
    }
}
