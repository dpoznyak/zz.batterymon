# Battery discharge rate monitor

## Usage

Tweak `appsettings.json` and restart the app.

## Images

Used [http://convertico.com/svg-to-ico/] to convert SVG to ICO.

# Copyrights

[LICENSE.md]
## Icons
